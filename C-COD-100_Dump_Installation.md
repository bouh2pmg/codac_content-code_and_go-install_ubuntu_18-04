---
module:             C-COD-100
title:              Dump Installation
subtitle:           Ubuntu 18.04

author:             Enzo Nicoletti
version:            1.1

---

#newpage

# Summary

## 1. Access to Internet
## 2. VirtualBox Installation
## 3. Creating a Virtual Machine
## 4. Install Ubuntu 18.04 in VirtualBox
## 5. Installing Softwares
* Updating your system
* Compilation Packages
* Install some IDE

#newpage

# Access to Internet

To begin the dump installation, you'll need an internet connection. If you decide to dump directly at CodingAcademy office, follow the next instruction to access to internet.
* Choose “Ionis Portal” network. A popup should appear asking an authentication.
* Enter your “socks” login and password, then wait for the redirection.
* If nothing appears, relaunch your browser and repeat point 2.

&nbsp;

# VirtualBox Installation

First, you’ll need to install VirtualBox. Go to [VirtualBox official page](ttps://www.virtualbox.org/wiki/Downloads), and download the latest stable version for windows. (Just click on the “Windows Host” link).

Then, just follow the installation. You may need to reconnect to the network during VirtualBox installation.

#warn(All in this document will redirect you to some manual pages to find what you have to do. You should always do your research in English only. You can be sure that assistants will not help you if you did not research your problem in English before calling them.)

To change language settings, click on Fichier > Preferences > Langage > English (Native).

#newpage

# Creating a Virtual Machine
## Setting up your first VM

Open VirtualBox and click “new” to create a new VM, then click on “Expert Mode” on the bottom panel. Then:
* Now, choose in the “name” field a name for your VM
* “Type” field must be “Linux”
* “Version” field must be “Ubuntu_64”
* “Memory Size” must be set at 8192MB (If your computer doesn't permiy it, set this to a lower value. If your computer has 8GB of RAM, set it at 6144MB)

#imageCenter(./img/vm1.png, 450px, 11)

Now, you just have to set your partition size (we highly recommended you to set it at least at 40GB,
  and select option VDI (VirtualBox Disk Image), with on the bottom-right panel the option Dynamically allocated (very important).

#imageCenter(./img/vm2.png, 450px, 11)

Now click on create and congrats ! You have your first virtual machine created !

&nbsp;

Back on the VirtualBox homepage, open File > 	Preferences > Network and click on “Add” button on the right-border panel. This should create a new NatNetwork, like the following screenshot.

#imageCenter(./img/vm3.png, 450px, 11)

Now, go on your VM settings by right-click on it, then Settings > Network.
You can now choose the NAT Network type and choose the NAT Network you just created.

#imageCenter(./img/vm4.png, 450px, 11)

#newpage

# Install Ubuntu 18.04 in VirtualBox
## Download Ubuntu

You have now to download an “Disk Image” to the official Ubuntu homepage.
  Please go to [Ubuntu official website](https://www.ubuntu.com/download/desktop) and download the 64-bits latest version of Ubuntu 18.04 (Usually named **Ubuntu 18.04.X LTS**).

## Ubuntu 18.04 in your VM

Start your virtual machine. VirtualBox should ask you a virtual optical disk file.
Click on Folder button and insert the Ubuntu Image your previously download, then click on start.

#imageCenter(./img/vm5.png, 450px, 11)

Your VM is started ! After few minutes, your Ubuntu should be launch and should ask you to select a language.

Select “English”, then “Install Ubuntu”.
You can now continue the installation with the following screenshot.

#imageCenter(./img/step1.png, 450px, 11)

#imageCenter(./img/step2.png, 450px, 11)

#imageCenter(./img/step3.png, 450px, 11)

#imageCenter(./img/step4.png, 450px, 11)

The installation should ask you personal information, like the name you want to give to your computer, your username, etc... 
Feel free to fill it by your own.

When the installation ends, just restart your VM and you’re ready to work !

#newpage

# Installing Softwares

## Updating your system

First, you’ll have to open a terminal. Open the application menu on the left-bottom corner and select “Terminal” (Or use the command Ctrl + Alt + T ).
A black window should open, like the following one:

#imageCenter(./img/temrinal.png, 450px, 11)

First of all, type the following command:

```
sudo apt-get update && sudo apt-get upgrade
```

This command will search new update on your system packages and applied them. 

#hint(It's important when you're working on a unix system to regulary update your system. Think about it !)

## Compilation Packages

Then, install the necessary package for compilation. (gcc, g++, etc…). That will be necessary to compile your C files during first week at CodingAcademy. 

Type, in your terminal again, the following command:

```
sudo apt-get install -y build-essential curl libbz2-dev
```

## Install some IDE

Now, it’s time to install your first IDE (Integrated development environment).

Defaults IDE are “nano”, “gedit” and “vi”, but there are others popular you have to install:

* [Emacs](https://www.gnu.org/software/emacs/):
    ```
    sudo apt-get install -y emacs php-elisp
    ```

* [Vim](https://www.vim.org/docs.php):
    ```
    sudo apt-get install vim
    ```

* [Sublime Text](https://www.sublimetext.com/3):

#terminal(A lancer : 
    wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
    sudo apt-get install apt-transport-https
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee etc/apt/sources.list.d/sublime-text.list
    sudo apt-get update && sudo apt-get install sublime-text)

You are now ready to start your formation at Coding Academy ! 